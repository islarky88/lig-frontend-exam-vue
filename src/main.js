import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";
import { routes } from "./routes.js";
import FlashMessage from "@smartweb/vue-flash-message";
Vue.use(FlashMessage, {
  strategy: "multiple"
});

Vue.use(VueRouter);

const router = new VueRouter({
  base: "/lig/frontend-vue/",
  routes,
  mode: "history"
});

new Vue({
  el: "#app",
  router,
  render: h => h(App)
});
