import SignUp from "./components/SignUp.vue";
import Messenger from "./components/Messenger.vue";
import SendMessage from "./components/message/SendMessage.vue";
import Footer from "./components/layout/Footer.vue";
import Header from "./components/layout/Header.vue";

export const routes = [
  {
    path: "",
    components: {
      default: SignUp,
      "header-top": Header,
      "footer-bot": Footer
    }
  },
  {
    path: "/messenger",
    components: {
      default: Messenger,
      "send-message": SendMessage,
      "header-top": Header
    }
  }
];
